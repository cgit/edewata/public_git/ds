======================================================================= 
                       389 Directory Server 1.2.1
=======================================================================

The 389 Directory Server is subject to the terms detailed in the
license agreement file called LICENSE.txt.

Late-breaking news and information on the 389 Directory Server is
available at the following location:

    http://port389.org/
